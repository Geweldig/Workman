#!/bin/bash
function workspace() {
    local DIR=~/.workspaces
    local EXITCODE=0

    if [[ ! -d $DIR ]]; then
        mkdir $DIR
    fi

    __workspaceman_add() {
        if [[ -f $DIR/.$1 ]]; then
            echo "Workspace with this name already exists, overwrite? "
            select yn in "Yes" "No"; do
                case $yn in
                    Yes ) break;;
                    No ) return $EXITCODE;;
                esac
            done
        fi
        pwd > $DIR/.$1
        EXITCODE=0
    }

    __workspaceman_info() {
        if [[ -f $DIR/last_used ]]; then
            local LU_NAME=$(tail -n 1 $DIR/last_used)
            local LU_PATH=$(head -n 1 $DIR/last_used)
            echo "Last used: $(tput setaf 2)${LU_NAME}$(tput sgr0) (${LU_PATH})"
        fi
        if [[ -f $DIR/prev_used ]]; then
            local PU_NAME=$(tail -n 1 $DIR/prev_used)
            local PU_PATH=$(head -n 1 $DIR/prev_used)
            echo "Prev used: $(tput setaf 2)${PU_NAME}$(tput sgr0) (${PU_PATH})"
        fi
    }

    __workspaceman_delete() {
        if [[ -f $DIR/.$1 ]]; then
            rm $DIR/.$1
            EXITCODE=0
        else
            EXITCODE=1
        fi
    }

    __workspaceman_list() {
        ls -A $DIR | grep ^\\. | sed s/\.//
        EXITCODE=0
    }

    __workspaceman_help() {
        cat "$DIR/workspace/help"
        EXITCODE=0
    }

    __workspaceman_version() {
        cat "$DIR/workspace/version"
        EXITCODE=0
    }

    __workspaceman_which() {
        if [[ -f $DIR/.$1 ]]; then
            cat $DIR/.$1
            EXITCODE=0
        else
            EXITCODE=1
        fi
    }

    # Handle flags, getops breaks for functions
    if [[ ! -z "$1" ]] && [[ $1 =~ ^-(-)?([a-zA-Z]) ]]; then

        # Handle -a/--add flag
        if [[ "$1" = "-a" ]] || [[ "$1" = "--add" ]]; then
            # Second parameter is required
            if [[ ! -z "$2" ]]; then
                __workspaceman_add $2
            else
                echo "A name is required when adding a new workspace."
                EXITCODE=1
                return $EXITCODE
            fi
            return $EXITCODE

        # Handle -l/--list flag
        elif [[ "$1" = "-l" ]] || [[ "$1" = "--list" ]]; then
            __workspaceman_list
            return $EXITCODE

        # Handle -h/--help flag
        elif [[ "$1" = "-h" ]] || [[ "$1" = "--help" ]]; then
            __workspaceman_help
            return $EXITCODE

        # Handle -v/--version flag
        elif [[ "$1" = "-v" ]] || [[ "$1" = "--version" ]]; then
            __workspaceman_version
            return $EXITCODE

                # Handle -i/--info flag
        elif [[ "$1" = "-i" ]] || [[ "$1" = "--info" ]]; then
            __workspaceman_info
            return $EXITCODE


        # Handle -d/--delete flag
        elif [[ "$1" = "-d" ]] || [[ "$1" = "--delete" ]]; then
            # Second parameter is required
            if [[ ! -z "$2" ]]; then
                __workspaceman_delete $2
            else
                echo "A name is required when deleting a workspace."
                EXITCODE=1
                return $EXITCODE
            fi
            return $EXITCODE

        # Handle -w/--which flag
        elif [[ "$1" = "-w" ]] || [[ "$1" = "--which" ]]; then
            # Second parameter is required
            if [[ ! -z "$2" ]]; then
                __workspaceman_which $2
            else
                echo "A name is required when checking a workspace directory."
                EXITCODE=1
                return $EXITCODE
            fi
            return $EXITCODE
        fi

    # Handle all other commands (i.e, switching directory)
    elif [[ ! -z "$1" ]] && [[ -f $DIR/.$1 ]]; then
        cd "$(head -n 1 $DIR/.$1)"
        mv $DIR/last_used $DIR/prev_used 2> /dev/null
        cp $DIR/.$1 $DIR/last_used 2> /dev/null
        echo $1 >> $DIR/last_used
        return $EXITCODE
    elif [[ ! -z "$1" ]] && [[ "$1" = "-" ]] && [[ -f $DIR/prev_used ]]; then
        cd "$(head -n 1 $DIR/prev_used)"
        mv $DIR/last_used $DIR/last_used.tmp 2> /dev/null
        mv $DIR/prev_used $DIR/last_used 2> /dev/null
        mv $DIR/last_used.tmp $DIR/prev_used 2> /dev/null
        return $EXITCODE
    elif [[ ! -z "$1" ]] && [[ "$1" = "-" ]] && [[ ! -f $DIR/prev_used ]]; then
        echo "There is no previously used workspace"
        EXITCODE=1
        return $EXITCODE
    elif [[ ! -z "$1" ]] && [[ ! -f $DIR/.$1 ]]; then
        echo "workspace not found: $1"
        EXITCODE=1
        return $EXITCODE
    elif [[ -f $DIR/last_used ]]; then
        cd "$(head -n 1 $DIR/last_used)"
        return $EXITCODE
    fi

    EXITCODE=1
    return $EXITCODE
}
